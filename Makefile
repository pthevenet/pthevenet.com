.PHONY: all

all: check test build

# check for dependencies
check:
	@if ! [ -x "$$(command -v hugo)" ];\
	then\
		echo "Hugo is missing (https://github.com/gohugoio/hugo/releases/download)";\
	else\
		hugo version;\
	fi

update:
	curl --header "PRIVATE-TOKEN: $(GITLAB_READ_TOKEN)" "https://gitlab.com/api/v4/projects/21823696/jobs/artifacts/master/raw/cv.pdf?job=build-tex" --output static/documents/cv.pdf

test:

build:
	@echo "Generating site"
	hugo --gc --minify

clean:
