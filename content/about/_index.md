---
title: "About"
date: 2023-08-03
draft: false
meta: false
dropCap: false
---

## About me 

I am a french/swiss late-20s engineer from the Lyon/Geneva regions. I studied computer science (technically communication systems) at [EPFL](https://www.epfl.ch). I specialize in DevOps. In work, I enjoy doing things right, improving things, and to keep learning (it's never enough ∞).

- 📧 <pierre@pthevenet.com>
- 📌 Lyon area, France
- 📄 [CV](/documents/about/cv.pdf)
- 💼 [LinkedIn](https://www.linkedin.com/in/pthevenet)
- 🔗 [Gitlab](https://gitlab.com/pthevenet) and [Github](https://github.com/pthevenet)
- 🌐 [pthevenet.com](https://pthevenet.com)

<div style="text-align:center"><img style="max-width:40%;" src="/pictures/photo_circle.png" /></div>

##### Education

- Master of Science MSc in Communication Systems (2020, EPFL)
- Bachelor of Science BSc in Communication Systems (2017, EPFL)
- Baccalauréat Général Série Scientifique (2013.)

##### Work experience

- [Jacquet Metals](https://www.jacquetmetals.com/en/): DevOps Systems Engineer (2024-)
- [Exotec](https://www.exotec.com): DevOps R&D Engineer (2024 Spring)
- [Elca Cloud Services](https://www.elcacloudservices.ch/en): DevOps Engineer (2021-2023)
- [NEC Laboratories Europe](https://www.neclab.eu): Master Thesis Intern (2020 Spring)
- [CERN Security Operations Center](https://cern.ch/security): Intern (2019 Summer)
- [Cisco Systems](https://www.cisco.com): Software Engineer Intern (2018 Spring)

##### Certifications/Training

- [Certified Kubernetes Administrator](/documents/about/cka.pdf) (2024, CNCF)
- [Foundations of Positive Psychology](https://coursera.org/share/97b7bd9df278eca483385ae252e6dc54) (2023, University of Pennsylvania)
- [Site Reliability Engineering: Measuring and Managing Reliability](https://coursera.org/share/d8eb8558008442b940a7957d7d1eb79f) (2023, Google Cloud)
- [Ceph Intensive Training](/documents/about/cert-ceph-intensive-training.pdf) (2021, Croit)
- [Functional Programming Principles in Scala](https://coursera.org/share/119f76e674bc6bcf698804b6d8ded033) (2017, EPFL)

## More about me

- 🎵 My favourite artist is Mark Knopfler, my favorite writer is Søren Kierkegaard.
- 🎲 I like board games and computer games.
- ⚙ I like to learn and read about philosophy, theology and psychology.
- 🌍 I lived most of my life near Lyon, as well as in the Geneva/Lausanne area. I lived in South Korea for nine months during my bachelor, and lived 6 months in Heidelberg during the covid-19 pandemic for my master thesis.
- 🌟 [Via survey of character strengths](https://ppc.sas.upenn.edu/resources/questionnaires-researchers/survey-character-strengths) top five:
  : caution + judgement + social-intelligence + modesty + honesty.

## This site

This site is made with [hugo](https://gohugo.io/).
The website's theme was adapted from the hugo theme [terminal](https://github.com/panr/hugo-theme-terminal), made by [panr](https://github.com/panr). Many thanks ☺.

The website's source code can be found [here](https://gitlab.com/pthevenet/pthevenet.com).
