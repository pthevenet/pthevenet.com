---
title: "Three good things exercise"
date: 2020-08-03
draft: false
dropCap: false
categories:
- psychology
tags:
- psychology
- personal
---

Recently, I completed a series of courses on the foundations of Positive Psychology.
I learned about the 'three good things' exercise from one of the courses.
This exercise consists of remembering three good things that happened in the day, every day.
I do that before going to sleep in the evening; I set up a reminder on my calendar for that.
After trying that exercise for a month, I found that it had a high impact on my well-being.
I am quite impressed in that regard, and that is why I wanted to share the experience and my thoughts.

During the month, I found that I spent more time thinking about the good events of the past during the day, and not only during the exercise practice.
This was automatic; I did not force myself.
So I want to say that practicing this exercise helps me focus on what's happening right now.
Being an engineer, it is usual for me to have to think about what can go wrong in order for me to prevent it proactively.
This is very helpful in my job, but is it helpful in my personal life?
I thought so, but I am more and more convinced that too much critical thinking is bad for well-being.
Remembering the good moments helps me to put things into perspective and to cope with difficulties.

Optimism is linked with better health, psychological and physical.
You can find interesting reads at the end of the article on that subject.
It seems to me that optimism can be learned, or at least practiced.
Because we tend to focus on our strengths rather than our weaknesses, I believe it is important to practice the weakly trained emotions, such as, in my case, hopefulness.
We may live happier and face difficult problems more easily.

Meditation is also an exercise that can improve well-being, but I did not manage to make it work for me.
I will try it again later.

---

##### Sources and related articles

- Optimism and your health: https://www.health.harvard.edu/heart-health/optimism-and-your-health
- Scheier, M. F., & Carver, C. S. (1992). Effects of optimism on psychological and physical well-being: Theoretical overview and empirical update. Cognitive therapy and research, 16(2), 201-228. [Link to the article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2894461/pdf/CPEMH-6-25.pdf).
- Cohn, M. A., & Fredrickson, B. L. (2010). In search of durable positive psychology interventions: Predictors and consequences of long-term positive behavior change. The journal of positive psychology, 5(5), 355-366. [Link to the article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3122474/)
- Gander, F., Proyer, R. T., Ruch, W., & Wyss, T. (2013). Strength-based positive interventions: Further evidence for their potential in enhancing well-being and alleviating depression. Journal of happiness studies, 14, 1241-1259. [Link to the article](https://doc.rero.ch/record/317443/files/10902_2012_Article_9380.pdf)

