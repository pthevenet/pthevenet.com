---
title: "Credit Risk Notes"
date: 2023-06-28
draft: false
dropCap: false
categories:
  - learning
tags:
  - explore
  - risk
---

I followed a course on credit risk management by TU Delft that you can find on [edx.org](https://www.edx.org/course/introduction-to-credit-risk-management).
As I often do, I summarized important points and searched for more content, all in a condensed notes PDF that you can find here.

{{< button Download "/documents/learning/credit_risk.pdf">}}

{{< pdfviewer "/documents/learning/credit_risk.pdf" >}}
