---
title: "Automated Web of Trust"
date: 2020-04-26T17:45:54+02:00
draft: false
dropCap: false
categories:
- projects
tags:
- decentralization
- security
---

Automating the sharing of keys in a decentralized network, based on the [PGP Web of Trust](https://pdfs.semanticscholar.org/e9aa/5d8032c1d925ea6a02dd3be93f42e831c965.pdf).


- 📄 [Report](https://github.com/No-Trust/doc/raw/master/doc/write_up.pdf)
- 📁 [Code](https://github.com/No-Trust/peerster/tree/master/awot)

