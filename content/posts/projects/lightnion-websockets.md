---
title: "Lightnion Websockets"
date: 2020-04-26T18:25:21+02:00
draft: false
dropCap: false
categories:
- projects
tags:
- privacy
---

Extending [Lightnion](https://github.com/spring-epfl/lightnion) with [Tor](https://www.torproject.org/)-redirected websockets.

The goal of the lightnion project is to provide network and application level anonymity for web applications, in any web browser. The end goal is to provide anomymity protection for students using online classroom tools.


- 📄 [Report](/documents/projects/lightnion-websockets/report.pdf)
- 💬 [Presentation](/documents/projects/lightnion-websockets/presentation.pdf)
- 📁 [Code](https://github.com/spring-epfl/lightnion)

Project done for the [SPRING Lab](https://www.epfl.ch/labs/spring) at EPFL.