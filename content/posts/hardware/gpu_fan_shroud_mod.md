---
title: "GPU fan shroud mod"
date: 2023-08-05
draft: false
dropCap: false
categories:
- hardware
tags:
- hardware
---

One year ago I bought a RX6650xt for my linux desktop, specifically the MSI MECH 2X version (it was cheap).
I had issues with noise when doing graphics-intensive tasks, but the performance was sufficient, so I did not want to buy another card.
I looked online for potential solutions, and I learned about the GPU fan shroud mod, and I wanted to try it!

Here are the results!


![](/images/hardware/gpu-fan-shroud-mod-front.jpg)

![](/images/hardware/gpu-fan-shroud-mod-case.jpg)

As you can see, this is a very simple mod, I simply replaced the gpu shroud with two noctua 120mm static-pressure optimized fans.
I did not have to remove the GPU heatsink, I read that for some GPUs it is necessary.
I attached the fans to the card with zip-ties in the PCIe bracket, GPU backplate and between the fans.
The structure is less rigid than before, but it holds its weight very well.
The fans are plugged into the motherboard, and the temperature is controlled through the PCIe temperature probe. This is not ideal, but this is the simplest way.

##### Results

The computer is way quieter during load.
Unfortunately, I lost my temperature measurements, but I tweaked the fan curves so that the GPU during load is at a lower temperature than before the mod.
I targeted a junction temperature of <85°C, which is largely in-spec (<110°C).
The fans do not have to go to 1000+ RPMs to efficiently cool the GPU, so the noise they produce is incredibly less irritating to my ears.
However, due to the fact that the fan speed is not controlled by the GPU, the idle noise is present but less annoying.

The graphics card used to be 2-slots, but the mod now makes it 3-slots.

In the end I am very happy with this mod, and I recommend it.
This mod can help save e-waste, as fans are quite reliable, I may reuse them as case fans when it is time to upgrade the GPU (in 7+years hopefully). Also, if you can reuse old fans, the mod is even better :).

##### Links

- Product page: https://www.msi.com/Graphics-Card/Radeon-RX-6650-XT-MECH-2X-8G-OC
- GPU fan shroud mod: https://linustechtips.com/topic/1151035-custom-gpu-shroud/