---
title: "Indexing a map with byte slices"
date: 2020-05-01T14:44:51+02:00
draft: false
dropCap: false
categories:
- programming
tags:
- go
- benchmark
---

## TLDR

Since `map[[]byte]_` are not possible in go, use `map[string]_` and index your byte slices using `string(byteSlice)`.

## Need for byte slice map keys

Go's default [`hash.Hash`](https://golang.org/pkg/hash/#Hash) interface returns hashes as slices of bytes.

```go
type Hash interface {
    // ...
    Sum(b []byte) []byte
    // ...
}
```

As we often use cryptographic hashes to identify complex objects, associative arrays with hashes as keys are needed to provide efficient insertion and retrieval of those objects.

## Problem

In go, associative arrays are provided by the language through the `map` construct.

However, byte slices cannot be used as a map key, as explained in the [language specification](https://golang.org/ref/spec#Map_types). Operators `==` and `!=` must be defined for the map key type.
Since slices are [descriptors of an underlying array](https://blog.golang.org/slices-intro), equality is ambiguous different as multiple definitions can exist.
This explains the lack of a standard definition in the language.

## Comparisons of possible solutions

So how can we still use byte slices as indexes to our maps ? Possibilities are:
- avoid slices, use arrays if possible
- convert the byte slices to a key type using a one-to-one function, such as [`string()`](https://golang.org/ref/spec#Conversions), or [`hex.EncodeToString()`](https://golang.org/pkg/encoding/hex/#EncodeToString)
- implement a custom map

Here is a benchmark of some possibilities, testing insertions, retrievals and deletions:
- `BenchmarkArrayKeyed`: use byte array as a key
- `BenchmarkStringKeyed`: convert byte slice to a key using [`string()`](https://golang.org/ref/spec#Conversions)
- `BenchmarkOptimizedStringKeyed`: convert byte slice to a key using [`string()`](https://golang.org/ref/spec#Conversions), and leverage an optimization described below
- `BenchmarkHexgKeyed`: convert byte slice to a string using [`hex.EncodeToString()`](https://golang.org/pkg/encoding/hex/#EncodeToString).


```
BenchmarkArrayKeyed-8             	19557058	        60.6 ns/op
BenchmarkOptimizedStringKeyed-8   	17753274	        67.7 ns/op
BenchmarkStringKeyed-8            	11834193	        102 ns/op
BenchmarkHexKeyed-8               	 6212077	        192 ns/op
```

Benchmark code is available [here](https://gitlab.com/pthevenet/go-small-answers/-/blob/29c03407006a269f8f6ba27fa3123fb9742ddf7c/byte-slice-indexed-maps/benchmark_test.go).

As one can see, using the built-in `string()` conversion with the optimization leads to a very small performance loss compared to using byte arrays.
Since we expect to use the `Hash` interface, the optimized `string()` conversion is to prefer.

Below I explain what is the optimized `string()` conversion.

## Optimization

Cases of `v := m[string(byteSlice)]` can be optimized by the go compiler. The optimization was introduced [here](https://github.com/golang/go/commit/f5f5a8b6209f84961687d993b93ea0d397f5d5bf#diff-3437cd20ec7506421b8d8b653efa9bfe
).

In short, internally the compiler can construct a temporary unsafe string from the byte slice, because it knows it will only be used as an index:

```goc
func slicebytetostringtmp(b Slice) (s String) {
    // ...
	s.str = b.array;
	s.len = b.len;
} 
```

This explains the difference in running times of:

```go
// string only used as an index
v := m[string(byteSlice)]
```

```go
// string may be used later
key := string(byteSlice)
v := m[key]
```
